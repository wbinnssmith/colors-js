(function (NS, _) {
  var colors = NS.colors = { };

  var conversions = {
    html2rgb:function (value) {
      var startPos = value[0] === '#' ? 1 : 0;

      var r = parseInt(value.slice(startPos,startPos+2), 16);
      startPos+=2;
      var g = parseInt(value.slice(startPos,startPos+2), 16);
      startPos+=2;
      var b = parseInt(value.slice(startPos,startPos+2), 16);
      return {r: r, g: g, b: b};
    },
    rgb2xyz:function () {
      var color = arguments[0];
      if (color.r == undefined && arguments.length == 3) {
        color = { r:arguments[0], g:arguments[1], b:arguments[2] }
      }

      function x(c) {
        if (c > 0.04045) {
          c = Math.pow((c + 0.055) / 1.055, 2.4);
        } else {
          c = c / 12.92;
        }
        return c * 100;
      }

      var r = x(color.r / 255);
      var g = x(color.g / 255);
      var b = x(color.b / 255);

      return {
        x:r * 0.4124 + g * 0.3576 + b * 0.1805,
        y:r * 0.2126 + g * 0.7152 + b * 0.0722,
        z:r * 0.0193 + g * 0.1192 + b * 0.9505
      };
    },

    xyz2lab:function () {
      var color;
      if (arguments.length === 3) {
        color = { x:arguments[0], y:arguments[1], z:arguments[2] }
      } else {
        color = arguments[0];
      }

      var x = color.x / 95.047;
      var y = color.y / 100.000;
      var z = color.z / 108.883;

      function m(c) {
        if (c > 0.008856) {
          c = Math.pow(c, 1 / 3);
        } else {
          c = (7.787 * c) + (16 / 116);
        }
        return c;
      }

      x = m(x);
      y = m(y);
      z = m(z);

      return {
        l:(116 * y) - 16,
        a:500 * (x - y),
        b:200 * (y - z)
      }
    },

    rgb2lab:function () {
      return this.xyz2lab(this.rgb2xyz.apply(this, arguments));
    },

    lab2rgb:function () {
      return this.xyz2rgb(this.lab2xyz.apply(this, arguments));
    },

    lab2xyz:function (color) {
      var color;
      if (arguments.length === 3) {
        color = { l:arguments[0], a:arguments[1], b:arguments[2] }
      } else {
        color = arguments[0];
      }

      var y = (color.l + 16) / 116;
      var x = color.a / 500 + y;
      var z = y - color.b / 200;

      function m(c) {
        if (Math.pow(c, 3) > 0.008856) {
          c = Math.pow(c, 3);
        } else {
          c = (c - 16 / 116) / 7.787;
        }
        return c;
      }

      x = m(x) * 95.047;
      y = m(y) * 100;
      z = m(z) * 108.883;

      return {
        x:x,
        y:y,
        z:z
      }
    },

    xyz2rgb: function () {
      var color;
      if (arguments.length === 3) {
        color = { x:arguments[0], y:arguments[1], z:arguments[2] }
      } else {
        color = arguments[0];
      }

      var x = color.x / 100;
      var y = color.y / 100;
      var z = color.z / 100;

      var r = x * 3.2406 + y * -1.5372 + z * -0.4986;
      var g = x * -0.9689 + y * 1.8757 + z * 0.0415;
      var b = x * 0.0557 + y * -0.2040 + z * 1.0570;

      function m(c) {
        if (c > 0.0031308) {
          c = 1.055 * Math.pow(c, (1 / 2.4)) - 0.055;
        } else {
          c = 12.92 * c;
        }
        return c;
      }

      r = m(r);
      g = m(g);
      b = m(b);

      return {
        r: Math.min(255, Math.max(0, Math.round(r * 255))),
        g: Math.min(255, Math.max(0, Math.round(g * 255))),
        b: Math.min(255, Math.max(0, Math.round(b * 255)))
      }
    },

    hsv2rgb: function () {
      var color;
      if (arguments.length === 3) {
        color = { h:arguments[0], s:arguments[1], v:arguments[2] }
      } else {
        color = arguments[0];
      }

      var
        h = color.h,
        v = color.v,
        s = color.s,
        c = v * s,
        x = c * (1 - Math.abs((h / 60) % 2 - 1)),
        m = v - c,
        rgb;

      if (h < 60) {
        rgb = [c, x, 0];
      } else if (h < 120) {
        rgb = [x, c, 0];
      } else if (h < 180) {
        rgb = [0, c, x];
      } else if (h < 240) {
        rgb = [0, x, c];
      } else if (h < 300) {
        rgb = [x, 0, c];
      } else if (h < 360) {
        rgb = [c, 0, x];
      }
      rgb[0] += m;
      rgb[1] += m;
      rgb[2] += m;

      rgb[0] = (rgb[0] * 255)|0;
      rgb[1] = (rgb[1] * 255)|0;
      rgb[2] = (rgb[2] * 255)|0;
      return { r: rgb[0], g: rgb[1], b: rgb[2]};
    },

    rgb2hsv: function () {
      var color;
      if (arguments.length === 3) {
        color = { r:arguments[0], g:arguments[1], b:arguments[2] }
      } else {
        color = arguments[0];
      }

      var
        r = color.r,
        g = color.g,
        b = color.b,
        M = Math.max(r,g,b),
        m = Math.min(r,g,b),
        c = M - m,
        h = 0;
      if (c === 0) {
        h = 0;
      } else if (M === r) {
        h = ((g - b) / c) % 6;
      } else if (M === g) {
        h = ((b - r) / c) + 2;
      } else if (M === b) {
        h = ((r - g) / c) + 4;
      }
      h = ((h * 60) + 360) % 360;

      return {h: h, s: c/M, v: M / 255};
    },

    lab2msh:function () {
      var color;
      if (arguments.length === 3) {
        color = { l:arguments[0], a:arguments[1], b:arguments[2] }
      } else {
        color = arguments[0];
      }

      var m = Math.sqrt(Math.pow(color.l, 2) + Math.pow(color.a, 2) + Math.pow(color.b, 2));
      var s = m ? Math.acos(color.l / m) : 0;
      var h = Math.atan2(color.b, color.a);

      return {
        m:m,
        s:s,
        h:h
      }
    },

    msh2lab:function () {
      var color;
      if (arguments.length === 3) {
        color = { m:arguments[0], s:arguments[1], h:arguments[2] }
      } else {
        color = arguments[0];
      }

      var l = color.m * Math.cos(color.s);
      var a = color.m * Math.sin(color.s) * Math.cos(color.h);
      var b = color.m * Math.sin(color.s) * Math.sin(color.h);

      return {
        l:l,
        a:a,
        b:b
      }
    },

    rgb2msh:function () {
      return conversions.lab2msh(conversions.rgb2lab.apply(conversions, arguments));
    },

    msh2rgb:function () {
      return conversions.lab2rgb(conversions.msh2lab.apply(conversions, arguments));
    }
  };
  NS.colors.conversions = conversions;


  var Phi = (1+ Math.sqrt(5))/2;
  var InvPhi = 1/Phi;
  var Scale = function () {

  };

  Scale.prototype = {
    _rgb: function(n, q) {
      if (q) {
        n = ((n / q + 1) | 0) * q;
      }
      return this.scale(n);
    },
    rgbScale: function (n, q) {
      var colors = this._rgb(n,q);
      return this.tupleToRgb(colors);
    },
    rgbaScale: function (n, a, q) {
      var colors = this._rgb(n,q);
      return this.tupleToRgba(colors, a);
    },

    html: function(n, q, a) {
      var colors = this._rgb(n,q);
      if (a !== undefined) {
        return ['#', colors.r.toString(16), colors.g.toString(16), colors.b.toString(16), (a*255).toString(16)].join('');
      } else {
        return ['#', colors.r.toString(16), colors.g.toString(16), colors.b.toString(16)].join('');
      }

    },


    tupleToRgba: function (color, a) {
      return 'rgba(' + [color.r, color.g, color.b, a].join(',') + ')';
    },

    tupleToRgb: function (color) {
      return 'rgb(' + [color.r, color.g, color.b].join(',') + ')';
    }
  };


  var RangeScale = function () {
    this._resolution = 1024;
    this._init();
  };

  RangeScale.prototype = _.defaults({
    _init: function () {
      this._scale = this._createScale(this._fn.bind(this));
    },
    _createScale:function (fn) {
      var
        scale = [],
        resolution = this._resolution;

      for (var i = 0; i < resolution; i++) {
        var y = i / resolution;
        scale.push(fn(y));
      }
      return scale;
    },

    scale:function (n) {
      var
        resolution = this._resolution,
        color = (n * resolution) | 0;

      return this._scale[Math.max(Math.min(resolution - 1, color), 0)]
    },


    rgbLow:function () {
      return this.tupleToRgb(this._scale[0]);
    },

    rgbMedium:function () {
      return this.tupleToRgb(this._scale[511]);
    },

    rgbHigh:function () {
      return this.tupleToRgb(this._scale[1023]);
    },

    preview: function (canvas) {
      var context = canvas.getContext('2d');
      var width = canvas.width;
      var height = canvas.height;
      var rectWidth = width / 100;
      context.clearRect(0,0, width + 1, height + 1);
      //context.setTransform(1,0,0,1,0.5,0.5);
      for (var i = 0; i < 100; i++) {
        context.beginPath();
        context.fillStyle = this.rgbScale(i / 100);
        context.fillRect(i * rectWidth, 0, rectWidth * 1.2, height);
      }
    }

  }, Scale.prototype);


  var CategoryScale = function (options) {
    if (options) {
      this.max = options.max;
    }
  };

  CategoryScale.prototype = _.defaults({
    preview: function (canvas) {
      var context = canvas.getContext('2d');
      var width = canvas.width;
      var height = canvas.height;
      var rectWidth = width / 25;
      var max = this.max;

      context.clearRect(0,0, width + 1, height + 1);
      //context.setTransform(1,0,0,1,0.5,0.5);

      for (var i = 0; i < (max ? max : 25); i++) {
        context.beginPath();
        context.fillStyle = this.rgbScale(i);
        context.fillRect(i * rectWidth, 0, rectWidth * 0.9, height);
      }
    }

  }, Scale.prototype);


  var FixedCategoryScale = function(values, options) {
    if (values[0].r) {
      this.colors = values;
    } else if (values[0][0] === '#') {
      var result = [];
      for (var i = 0; i < values.length; i++) {
        result.push(conversions.html2rgb(values[i]));
      }
      this.colors = result;
    }

    this.max = values.length;

    CategoryScale.call(this, options);
  };

  FixedCategoryScale.prototype = _.defaults({
    scale: function(n) {
      return this.colors[n % this.max];
    }
  }, CategoryScale.prototype);


  var MshCategoryScale = function(base, options) {
    this.start_color = conversions.rgb2msh(base);
    CategoryScale.call(this, options);
  };

  MshCategoryScale.prototype = _.defaults({
    scale: function(n) {
      var
        color = InvPhi * (n|0) * Math.PI * 2 - Math.PI,
        start_color = this.start_color,
        h = ((start_color.h + color) % (2 * Math.PI)) - Math.PI,
        msh = { m: start_color.m, s: start_color.s, h: h};

      return conversions.msh2rgb(msh);
    }

  }, CategoryScale.prototype);

  var HsvCategoryScale = function(base,options) {
    this.start_color = conversions.rgb2hsv(base);
    CategoryScale.call(this, options);
  };

  HsvCategoryScale.prototype = _.defaults({
    scale: function(n) {
      var
        color = InvPhi * (n|0) * 360,
        start_color = this.start_color,
        h = (start_color.h + color) % 360;


      return conversions.hsv2rgb({h: h, s: start_color.s, v: start_color.v});
    }

  }, CategoryScale.prototype);



  var MshScale = function (from, to, midpoint, options) {
    this._from = conversions.rgb2msh(from);
    this._to = conversions.rgb2msh(to);
    this._midpoint = midpoint;

    RangeScale.call(this, options);
  };

  var MshProto = {
    _spinMshHue:function (sat, unsat) {
      //return { m: unsat.m, s: unsat.s, h: sat.h };

      if (sat.m >= unsat.m) {
        return { m:unsat.m, s:unsat.s, h:sat.h };
      }
      var spin = sat.s * Math.sqrt(Math.pow(unsat.m, 2) - Math.pow(sat.m, 2)) /
        (sat.m * Math.sin(sat.s));

      if (sat.h > (Math.PI / -3)) {
        return { m:unsat.m, s:unsat.s, h:sat.h + spin };
      } else {
        return { m:unsat.m, s:unsat.s, h:sat.h - spin };
      }
    },

    _fn: function (i) {
      var from = this._from;
      var to = this._to;

      // original algorithm also checked hue proximity:
      // RADDIFF(from.h, to.h) > Math.PI / 3

      if ((from.s > 0.05) && (to.s > 0.05)) {
        var mMid = Math.max(from.m, to.m, this._midpoint);
        if (i < 0.5) {
          to = { m:mMid, s:0, h:0 };
          i *= 2;
        } else {
          from = { m:mMid, s:0, h:0 };
          i = i * 2 - 1;
        }
      }

      if ((from.s < 0.05) && (to.s > 0.05)) {
        from = this._spinMshHue(to, from);
      } else if ((to.s < 0.05) && (from.s > 0.05)) {
        to = this._spinMshHue(from, to);
      }

      var ii = 1 - i;
      var result = {
        m:(from.m * ii + to.m * i),
        s:(from.s * ii + to.s * i),
        h:(from.h * ii + to.h * i)
      };

      return conversions.msh2rgb(result);
    }
  };

  MshScale.prototype = _.defaults(MshProto, RangeScale.prototype);


  var CubeHelixScale = function (offset, rate, options) {

    this.rate = rate;
    this.offset = offset;

    RangeScale.call(this, options);
  };


  CubeHelixScale.prototype = _.defaults({

    _fn: function (y) {
      var
        rate = this.rate,
        offset = this.offset,
        a = (rate * y + offset / 3.0 + 1) * 2 * Math.PI,
        s = 0.6 * y * (1 - y);

      var r = y + s * (-0.14861 * Math.cos(a) + 1.78277 * Math.sin(a));
      var g = y + s * (-0.29227 * Math.cos(a) - 0.90649 * Math.sin(a));
      var b = y + s * (+1.97294 * Math.cos(a));

      r = (r * 255 + 0.5)|0;
      g = (g * 255 + 0.5)|0;
      b = (b * 255 + 0.5)|0;
      if (r < 0) r = 0;
      if (r > 255) r = 255;
      if (g < 0) g = 0;
      if (g > 255) g = 255;
      if (b < 0) b = 0;
      if (b > 255) b = 255;

      return { r:r, g:g, b:b };
    }

  }, RangeScale.prototype);




  var schemes = [
    {
      type: 'range',
      name: 'msh-blue-red',
      fn: function () {
        return new MshScale({ r: 33, g: 102, b: 172 }, { r: 176, g: 24, b: 43 }, 95);
      }
    },
    {
      type: 'range',
      name: 'msh-green-red',
      fn: function () {
        return new MshScale({ r: 22, g: 136, b: 51 }, { r: 194, g: 55, b: 60 }, 95);
      }
    },
    {
      type: 'range',
      name: 'msh-wallboard',
      aliases: ['wallboard'],
      fn: function () {
        return new MshScale({ r: 31, g: 64, b: 106 }, { r: 240, g: 40, b: 40 }, 88);
      }
    },
     {
      type: 'range',
      name: 'msh-wallboard-dark',
      aliases: ['wallboard'],
      fn: function () {
        return new MshScale({ r: 40, g: 50, b: 140 }, { r: 140, g: 50, b: 40 }, 0);
      }
    },

    {
      type: 'range',
      name: 'cubehelix-default',
      aliases: ['cubehelix'],
      fn: function () {
        return new CubeHelixScale(0, 1);
      }
    },
    {
      type: 'range',
      name: 'cubehelix-reverse',
      aliases: ['cubehelix'],
      fn: function () {
        return new CubeHelixScale(-1.7, 1);
      }
    },
    {
      type: 'range',
      name: 'cubehelix-rainbow',
      aliases: ['cubehelix'],
      fn: function () {
        return new CubeHelixScale(0, 2);
      }
    },

    {
      type: 'category',
      name: 'hsv-default',
      fn: function () {
        return new HsvCategoryScale({ r: 240, g: 80, b: 80 });
      }
    },
    {
      type: 'category',
      name: 'msh-default',
      fn: function () {
        return new MshCategoryScale({r: 240, g: 80, b: 80});
      }
    },
    {
      type: 'category',
      name: 'hsv-pastel',
      fn: function () {
        return new HsvCategoryScale({r: 255, g: 192, b: 210});
      }
    },
    {
      type: 'category',
      name: 'msh-pastel',
      fn: function () {
        return new MshCategoryScale({r: 255, g: 192, b: 210});
      }
    },
    {
      type: 'category',
      name: 'hsv-atlassian',
      fn: function () {
        return new HsvCategoryScale(conversions.html2rgb('#3b7fc4'));
      }
    },
    {
      type: 'category',
      name: 'msh-atlassian',
      fn: function () {
        return new MshCategoryScale(conversions.html2rgb('#3b7fc4'));
      }
    },
    {
      type: 'category',
      name: 'adg-primary',
      fn: function() {
        return new FixedCategoryScale(['#205081','#3b73af','#333333','#f5f5f5','#ffffff']);
      }
    },
    {
      type: 'category',
      name: 'adg-secondary-lighter2',
      fn: function() {
        var adg = ['#3b7fc4','#ebf2f9','#707070','#14892c','#ffd351', '#815b3a','#d04437','#4a6785', '#654982'];
        adg = _.map(adg, function(color){
          var hsv = conversions.rgb2hsv(conversions.html2rgb(color));
          hsv.v = Math.min(hsv.v * 1.50, 1);
          hsv.s = Math.min(hsv.s * 0.16, 1);
          return conversions.hsv2rgb(hsv);
        });
        return new FixedCategoryScale(adg);
      }
    },
    {
      type: 'category',
      name: 'adg-secondary-lighter',
      fn: function() {
        var adg = ['#3b7fc4','#ebf2f9','#707070','#14892c','#ffd351', '#815b3a','#d04437','#4a6785', '#654982'];
        adg = _.map(adg, function(color){
          var hsv = conversions.rgb2hsv(conversions.html2rgb(color));
          hsv.v = Math.min(hsv.v * 1.15, 1);
          hsv.s = Math.min(hsv.s * 0.33, 1);
          return conversions.hsv2rgb(hsv);
        });
        return new FixedCategoryScale(adg);
      }
    },
    {
      type: 'category',
      name: 'adg-secondary',
      fn: function() {
        return new FixedCategoryScale(['#3b7fc4','#ebf2f9','#707070','#14892c','#ffd351', '#815b3a','#d04437','#4a6785', '#654982']);
      }
    },
    {
      type: 'category',
      name: 'adg-secondary-darker',
      fn: function() {
        var adg = ['#3b7fc4','#ebf2f9','#707070','#14892c','#ffd351', '#815b3a','#d04437','#4a6785', '#654982'];
        adg = _.map(adg, function(color){
          var hsv = conversions.rgb2hsv(conversions.html2rgb(color));
          hsv.v = Math.min(hsv.v * 0.75, 1);
          hsv.s = Math.min(hsv.s * 1.25, 1);
          return conversions.hsv2rgb(hsv);
        });
        return new FixedCategoryScale(adg);
      }
    },
    {
      type: 'category',
      name: 'adg-secondary-darker2',
      fn: function() {
        var adg = ['#3b7fc4','#ebf2f9','#707070','#14892c','#ffd351', '#815b3a','#d04437','#4a6785', '#654982'];
        adg = _.map(adg, function(color){
          var hsv = conversions.rgb2hsv(conversions.html2rgb(color));
          hsv.v = Math.min(hsv.v * 0.50, 1);
          hsv.s = Math.min(hsv.s * 1.25, 1);
          return conversions.hsv2rgb(hsv);
        });
        return new FixedCategoryScale(adg);
      }
    }
  ];


  var schemeLookup;
  var schemeInstances;

  function register(definition) {
    var type = definition.type;

    schemeLookup[type + ':' + definition.name] = definition;
    if (definition.aliases) {
      _.forEach(definition.aliases, function(alias) {
        schemeLookup[type + ':' + alias] = definition;
      });
    }
  }

  function init() {
    schemeLookup = {};
    schemeInstances = {};

    _.forEach(schemes, register);
  }

  NS.colors.getScheme = function (name) {
    if (! schemeLookup) {
      init();
    }
    var definition = schemeLookup[name];
    if (definition) {
      var schemeName = definition.name;
      if (! schemeInstances[schemeName]) {
        schemeInstances[schemeName] = definition.fn();
      }
      return schemeInstances[schemeName];
    }
  };

  NS.colors.getSchemeNames = function(type) {
    return _.chain(schemes)
      .filter(function(scheme) {
        return scheme.type === type;
      })
      .map(function(scheme) {
        return scheme.name;
      })
      .value();
  };

})(window.BB ? window.BB : window.BB = {}, _);
